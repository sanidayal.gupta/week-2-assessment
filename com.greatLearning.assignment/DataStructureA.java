package com.greatLearning.assignment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DataStructureA implements Comparator<Employee>{
	
	public void sortingNames(ArrayList<Employee> employees) {
		
		//Sorting Name
		Collections.sort(employees, new DataStructureA());
		
		//Storing Name into the list just for printing like this "[ , ]"
		ArrayList<String> name=new ArrayList<String>();
		for(Employee emp:employees) {
			name.add((emp.getName()));
		}
		System.out.print(name);
	}

	@Override
	public int compare(Employee o1, Employee o2) {
		// TODO Auto-generated method stub
		return o1.getName().compareTo(o2.getName());
	}	
}